# Dynamic-Programming Algorithms 

The implementation of the algorithms is in C++ but we are able to run the programs
through R ( Rcpp ). For further information considering these algorithms please
follow the links provided below:

1) Money(Coin) change problem - Implemented in Python
    reference link: https://www.geeksforgeeks.org/dynamic-programming-set-7-coin-change/

2) Edit distance - C++
    reference link: https://www.geeksforgeeks.org/dynamic-programming-set-5-edit-distance/

3) Binomial Coefficients calculation using DP ( + 2  R methods) - C++
   reference link: https://www.geeksforgeeks.org/dynamic-programming-set-9-binomial-coefficient/
    
